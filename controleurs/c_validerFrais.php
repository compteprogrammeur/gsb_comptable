<?php
/**
 * Gestion des frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */

$mois = getMois(date('d/m/Y'));

$numAnnee = substr($mois, 0, 4);
$numMois = substr($mois, 4, 2);
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$lesMois = array();

foreach($pdo->getVisiteurs() as $value)
{
    foreach($pdo->getLesMoisDisponibles($value['id']) as $value2)
    {
        array_push($lesMois, $value2);
    }
}

$lesMois = array_unique($lesMois, SORT_REGULAR);

require 'vues/v_listeVisiteurs.php';

switch ($action) {
case 'transmettreInformations':
    $visiteurChoisi = filter_input(INPUT_POST, 'lstVisiteur', FILTER_SANITIZE_STRING);
    $mois = filter_input(INPUT_POST, 'lstMois', FILTER_SANITIZE_STRING);
    $_SESSION['idVisiteur1'] = $visiteurChoisi;
    
    $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($visiteurChoisi, $mois);
    $lesFraisForfait = $pdo->getLesFraisForfait($visiteurChoisi, $mois);
    require 'vues/v_validerListeFraisForfait.php';
    require 'vues/v_validerListeFraisHorsForfait.php';
    
    /*if ($pdo->estPremierFraisMois($idVisiteur, $mois)) {
        $pdo->creeNouvellesLignesFrais($idVisiteur, $mois);
    }*/
    break;
case 'validerFicheFraisForfait':
    $lesFrais = filter_input(INPUT_POST, 'lesFrais', FILTER_DEFAULT, FILTER_FORCE_ARRAY);
    if (!isset($_SESSION['idVisiteur1']))
    {
        ajouterErreur('Faites un choix de visiteur.');
        include 'vues/v_erreurs.php';
    }
    else
    {
        if (lesQteFraisValides($lesFrais)) {
            $pdo->majFraisForfait($_SESSION['idVisiteur1'], $mois, $lesFrais);
        } else {
            ajouterErreur('Les valeurs des frais doivent être numériques');
            include 'vues/v_erreurs.php';
        }
    }
    break;
case 'validerFraisHorsForfait':
    $dateFrais = filter_input(INPUT_POST, 'dateFrais', FILTER_SANITIZE_STRING);
    $libelle = filter_input(INPUT_POST, 'libelle', FILTER_SANITIZE_STRING);
    $montant = filter_input(INPUT_POST, 'montant', FILTER_VALIDATE_FLOAT);
    $idFrais = filter_input(INPUT_GET, 'idFrais', FILTER_SANITIZE_STRING);
    
    valideInfosFrais($dateFrais, $libelle, $montant);
    if (nbErreurs() != 0) {
        include 'vues/v_erreurs.php';
    } else {
        if (isset($_SESSION['idVisiteur1']))
        {
            $pdo->supprimerFraisHorsForfait($idFrais);
            $pdo->creeNouveauFraisHorsForfait(
                $_SESSION['idVisiteur1'],
                $mois,
                $libelle,
                $dateFrais,
                $montant
            );
            echo '<br><br><center><p style="font: Calibri; font-size: 16px; color: green;">Les informations ont été mises à jour.</p></center>';
        }
    }
    break;
case 'supprimerFrais':
    $idFrais = filter_input(INPUT_GET, 'idFrais', FILTER_SANITIZE_STRING);
    $pdo->supprimerFraisHorsForfait($idFrais);
    break;
default:
    echo '<br><br><center><p style="font: Calibri; font-size: 16px;">Veuillez saisir les informations.</p></center>';
    break;
}
    
