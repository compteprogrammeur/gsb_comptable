<?php
/**
 * Gestion de l'affichage des frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */



$lesMois = array();

foreach($pdo->getVisiteurs() as $value)
{
    foreach($pdo->getLesMoisDisponibles($value['id']) as $value2)
    {
        array_push($lesMois, $value2);
    }
}

$lesMois = array_unique($lesMois, SORT_REGULAR);

$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

switch ($action) {
case 'selectionnerMois':
    require 'vues/v_listeVisiteurs2.php';
    break;
case 'voirEtatFrais':
    $leMois = filter_input(INPUT_POST, 'lstMois', FILTER_SANITIZE_STRING);
    $idVisiteur = filter_input(INPUT_POST, 'lstVisiteur', FILTER_SANITIZE_STRING);
    $_SESSION['lstMois'] = $leMois;
    $_SESSION['idVisiteur3'] = $idVisiteur;
            
    include 'vues/v_listeVisiteurs2.php';
    $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $leMois);
    $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $leMois);
    $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $leMois);
    $numAnnee = substr($leMois, 0, 4);
    $numMois = substr($leMois, 4, 2);
    $libEtat = $lesInfosFicheFrais['libEtat'];
    $montantValide = $lesInfosFicheFrais['montantValide'];
    $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
    $dateModif = dateAnglaisVersFrancais($lesInfosFicheFrais['dateModif']);
    include 'vues/v_etatFrais.php';
    include 'vues/v_suivreButton.php';
    break;
case 'miseEnPaiement':
    $mois1 = getMois(date('d/m/Y'));
    //$pdo->majEtatFicheFrais($_SESSION['idVisiteur3'], $mois1, 'VA');
    echo '<br><br><center><p style="font: Calibri; font-size: 16px; color: green;">La fiche a été mise en paiement.</p></center>';
    break;
}
