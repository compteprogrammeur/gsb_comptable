<?php

    require_once(realpath(__DIR__.'/../includes/fct.inc.php'));
    require_once(realpath(__DIR__.'/../includes/class.pdogsb.inc.php'));
    
    $pdo = PdoGsb::getPdoGsb();

    $login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
    $mdp = filter_input(INPUT_POST, 'mdp', FILTER_SANITIZE_STRING);
    $visiteur = $pdo->getInfosVisiteur($login, $mdp);
    
    $lesFrais = array();
    $lesFrais["ETP"] = (int)filter_input(INPUT_POST, 'etp', FILTER_SANITIZE_STRING);
    $lesFrais["NUI"] = (int)filter_input(INPUT_POST, 'nui', FILTER_SANITIZE_STRING);
    $lesFrais["KM"] = (int)filter_input(INPUT_POST, 'km', FILTER_SANITIZE_STRING);
    $lesFrais["REP"] = (int)filter_input(INPUT_POST, 'rep', FILTER_SANITIZE_STRING);
    
    $pdo->majFraisForfait($visiteur["id"], filter_input(INPUT_POST, 'date', FILTER_SANITIZE_STRING), $lesFrais);

    echo("It works.");

?>