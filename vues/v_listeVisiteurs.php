<?php
/**
 * Vue Liste des mois
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>




<div class="row">
    <div class="col-md-4">
        <h3>Faites votre selection : </h3>
    </div>
    <div class="col-md-4">
            
        
        <form action="index.php?uc=validerFrais&action=transmettreInformations" 
              method="post" role="form">
            <div class="form-group">
                
                <label for="lstVisiteur" accesskey="n">Visiteur : </label>
                <select id="lstVisiteur" name="lstVisiteur" class="form-control">
                <?php
                $visiteurs = $pdo->getVisiteurs();
                foreach ($visiteurs as $visiteur) {
                $nom = $visiteur['nom'];
                $prenom = $visiteur['prenom'];
                $id = $visiteur['id'];
                ?>
                <option value="<?php echo $id ?>">
                <?php echo $nom . ' ' . $prenom ?> </option>
                <?php

                }
                ?>    

                </select>
                
                <label for="lstMois" accesskey="n">Mois : </label>
                <select id="lstMois" name="lstMois" class="form-control">
                    <?php
                    foreach ($lesMois as $unMois) {
                        $mois = $unMois['mois'];
                        $numAnnee = $unMois['numAnnee'];
                        $numMois = $unMois['numMois'];
                        if ($mois == $moisASelectionner) {
                            ?>
                            <option selected value="<?php echo $mois ?>">
                                <?php echo $numMois . '/' . $numAnnee ?> </option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $mois ?>">
                                <?php echo $numMois . '/' . $numAnnee ?> </option>
                            <?php
                        }
                    }
                    ?>    

                </select>
            </div>
            <input id="ok" type="submit" value="Valider" class="btn btn-success" 
                   role="button">
        </form>
    </div>
</div>